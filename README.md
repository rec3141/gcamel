G+C Content Amelioration program (GCamel)
by R. Eric Collins

using (approximately) the method of:
Lawrence, J. G., & Ochman, H. (1997). Amelioration of bacterial genomes: rates of change and exchange. Journal of molecular evolution, 44(4), 383-397.

More info:
http://www.reric.org/wordpress/archives/1034
